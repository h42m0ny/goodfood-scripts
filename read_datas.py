import csv
import json 
import os
from pymongo import MongoClient

mongo_client = MongoClient(os.getenv('MONGODB_URL','mongodb://localhost:27017'))
db = mongo_client.goodfood
db.restaurants.drop()
db.clients.drop()

clients_fields = ('_id', 'name', 'firstname', 'email', 'phone_number', 'restaurants', 'address')
address_fields = ('line1', 'line2', 'city', 'zipcode', 'country')
restaurants_fields = ('_id', 'name', 'geo_coord', 'logo', 'description', 'phone_number', 'email', 'capacity', 'address', 'meals')
food_fields = ('name', 'type_food')
with open('./datas/restaurants.csv', encoding='utf-8', newline='') as csvfile:
    restaurants_reader = csv.DictReader(csvfile, delimiter=',')
    restaus = []
    for row in restaurants_reader:
        restau = {field: row[field] for field in restaurants_fields}
        restau['address'] = json.loads(row['address'])
        restau['meals'] = json.loads(row['meals'])
        #print(restau)
        restaus.append(restau)
db.restaurants.insert_many(restaus)


clients = []
with open('./datas/clients.csv', encoding='utf-8', newline='') as csvfile:
    clients_reader = csv.DictReader(csvfile, delimiter=',')
    for row in clients_reader:
        client = {field: row[field] for field in clients_fields}
        client['address'] = json.loads(row['address'])
        restaus_cli = json.loads(row['restaurants'])
        client["restaurants"] = restaus_cli
        # for id_restau in restaus_cli:
        #      client["restaurants"].append(db.restaurants.find_one({"_id": id_restau}))
        clients.append(client)
        print(clients)
db.clients.insert_many(clients)


    